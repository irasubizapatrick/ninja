<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidSupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bid__sups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('bid_id')->unsigned()->index();
            $table->foreign('bid_id')->references('id')->on('bids')->onDelete('cascade');
            $table->integer('supply_id')->unsigned()->index();
            $table->foreign('supply_id')->references('id')->on('supplies')->onDelete('cascade');
            $table->string('bid_start_date');
            $table->string('bid_end_date');
            $table->string('contract_file');
            $table->string('bid_sup_status')->default('active');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bid__sups');
    }
}
