<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LaratrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->command->info('Truncating User, Role and Permission tables');
        $this->truncateLaratrustTables();

        $config = config('laratrust_seeder.role_structure');
        $userPermission = config('laratrust_seeder.permission_structure');
        $mapPermission = collect(config('laratrust_seeder.permissions_map'));

        $Masterrole = \App\Role::create([
            'name' => 'master',
            'display_name' => 'Master',
            'description' => 'Master'
        ]);

        $Adminrole = \App\Role::create([
            'name' => 'admin',
            'display_name' => 'Admin',
            'description' => 'Admin'
        ]);

        $Ownerrole = \App\Role::create([
            'name' => 'owner',
            'display_name' => 'Owner',
            'description' => 'Owner'
        ]);
        $Clientrole = \App\Role::create([
            'name' => 'client',
            'display_name' => 'Client',
            'description' => 'Client'
        ]);



        $admin = \App\User::create([
            'name' => 'admin',
            'email' => 'admin@crm.com',
            'password' => bcrypt('password')
        ]);
        $admin->attachRole($Adminrole);


        $Master = \App\User::create([
            'name' => 'master',
            'email' => 'master@crm.com',
            'password' => bcrypt('password')
        ]);
        $Master->attachRole($Masterrole);

        $Client = \App\User::create([
            'name' => 'client',
            'email' => 'client@crm.com',
            'password' => bcrypt('password')
        ]);
        $Client->attachRole($Clientrole);

        $owner = \App\User::create([
            'name' => 'owner',
            'email' => 'owner@crm.com',
            'password' => bcrypt('password')
        ]);
        $owner->attachRole($Ownerrole);

    }

    /**
     * Truncates all the laratrust tables and the users table
     *
     * @return    void
     */
    public function truncateLaratrustTables()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('permission_role')->truncate();
        DB::table('permission_user')->truncate();
        DB::table('role_user')->truncate();
        \App\User::truncate();
        \App\Role::truncate();
        \App\Permission::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
