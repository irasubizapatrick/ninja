<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Meeting extends Model
{

    use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $table	=	'meetings';
    protected $fillable	=	['id', 'user_id', 'bid__sup_id','meeting_subject','meeting_comment','meeting_started_date','meeting_end_date'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *   Function belong to Call Model
     */
    public function bid_sup()
    {
        return $this->belongsTo('App\Bid_Sup');
    }
}
