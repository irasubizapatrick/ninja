<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bid_Sup extends Model
{

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'bid__sups';
    protected $fillable = ['id', 'user_id', 'bid_id', 'supply_id', 'bid_start_date', 'bid_end_date', 'bid_sup_status', 'contract_file'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *   Function belong to users  Model
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *   Function belong to Organization  Model
     */
    public function supply()
    {
        return $this->belongsTo('App\Supply');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *   Function belong to Organization  Model
     */
    public function organizer()
    {
        return $this->belongsTo('App\Organization');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *   Function belong to Organization  Model
     */
    public function bid()
    {
        return $this->belongsTo('App\Bids');
    }
}


