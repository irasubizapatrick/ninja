<?php

namespace App\Http\Controllers;

use App\Bid_Sup;
use App\Organization;
use App\Role;
use App\Supply;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->hasRole('master'))
        {
            $organization = Supply::all();
            dd($organization);
        }
        else if ($user->hasRole('owner'))
        {
            $organization = Supply::all();
            dd($organization);
        }
        else if ($user->hasRole('client'))
        {
            $organization = Supply::all();
            dd($organization);
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $role = Role::where('display_name', $request['role'])->first();

        $user = new User;
        $user->email = $request->get('email');
        $user->password = Hash::Make($request->get("password"));
        $user->save();

        $user->attachRole($role);

        if ($user->save())

        $user_id = $user->id;
        $request->merge(['user_id' => $user_id]);
        $all_data = Supply::create($request->all());
        $all_data->save();
        return dd($all_data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id     = Auth::id();
        $order       =  Supply::findOrFail($id);
        $request->merge(['user_id' => $user_id]);
        $all_data =  $request->all();
        $order->update($all_data);
        Session::flash('message', 'Lead Created Successful  successfully');
        return dd($order);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $meetings =Supply::findOrfail($id);
        $meetings->delete();
        return dd($meetings);
    }
}
