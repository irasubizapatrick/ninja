<?php

namespace App\Http\Controllers;

use App\Bid_Sup;
use App\Bids;
use App\Organization;
use App\Role;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;

class BidsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->hasRole('owner'))
        {
            $user_id      = Auth::id();
            $organization = Bids::select('bids.*')
                            ->join( 'organizations','organizations.id', '=', 'bids.organization_id')
                            ->where('organizations.user_id','=',$user_id)
                            ->get();
            return view('bids.list',compact('organization'));
        }
        elseif ($user->hasRole('master'))
        {
            $organization = Bids::select('bids.*')
                            ->join( 'organizations','organizations.id', '=', 'bids.organization_id')
                            ->get();
            return view('bids.list',compact('organization'));
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if ($user->hasRole('owner'))
        {
            $user_id = Auth::id();
            $get_company = Organization::whereuser_id($user_id)->first()->id;
            $request->merge(['user_id' => $user_id, 'organization_id' => $get_company]);
            $all_data = Bids::create($request->all());
            $all_data->save();
            return dd($all_data);
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        if ($user->hasRole('owner'))
        {
            $user_id = Auth::id();
            $order = Bids::findOrFail($id);
            $get_company = Organization::whereuser_id($user_id)->first()->id;
            $request->merge(['user_id' => $user_id, 'organization_id' => $get_company]);
            $all_data = $request->all();
            $order->update($all_data);
            Session::flash('message', 'Lead Created Successful  successfully');
            return dd($order);
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'],401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user = Auth::user();
        if ($user->hasRole('owner'))
        {
            $meetings = Bids::findOrfail($id);
            $meetings->delete();
            return dd($meetings);
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }
}
