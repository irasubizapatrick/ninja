<?php

namespace App\Http\Controllers;

use App\Bid_Sup;
use App\Organization;
use App\Reminder;
use App\Role;
use App\Supply;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;

class ReminderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->hasRole('master'))
        {
            $reminder = Reminder::with('bid_sup')
                        ->where('bid__sups.id', '=', 'reminders.bid__sup_id')
                        ->get();
            dd($reminder);
        }
        elseif ($user->hasRole('owner'))
        {
            $user_id = Auth::id();
            $reminder = Reminder::with('bid_sup')
                        ->where('bid__sups.id', '=', 'reminders.bid__sup_id')
                        ->where('reminders.user_id','=',$user_id)
                        ->get();
            dd($reminder);
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = Auth::user();
        if ($user->hasRole('owner'))
        {
                $user_id = Auth::id();
                $request->merge(['user_id' => $user_id]);
                $sup_data = Reminder::create($request->all());
                $sup_data->save();
                return dd($sup_data);
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = Auth::user();
        if ($user->hasRole('owner'))
        {
        $order       =  Reminder::findOrFail($id);
        $all_data =  $request->all();
        $order->update($all_data);
        return dd($order);
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        if ($user->hasRole('owner'))
        {
        $meetings = Reminder::findOrfail($id);
        $meetings->delete();
        return dd($meetings);
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }
}
