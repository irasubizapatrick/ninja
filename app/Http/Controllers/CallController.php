<?php

namespace App\Http\Controllers;

use App\Bid_Sup;
use App\Call;
use App\Meeting;
use App\Organization;
use App\Role;
use App\Supply;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;

class CallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();
        if ($user->hasRole('master'))
        {
            $call = Call::with('bid_sup')
                ->where('bid__sups.id', '=', 'calls.bid__sup_id')
                ->get();
            dd($call);
        }
        else if ($user->hasRole('owner'))
        {
            $user_id =Auth::id();
            $call = Call::with('bid_sup','user')
                    ->where('bid__sups.id', '=', 'calls.bid__sup_id')
                    ->where('bid__sups.user_id','=',$user_id)
                    ->get();
            dd($call);
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if ($user->hasRole('owner'))
        {
            $user_id = Auth::id();
            $request->merge(['user_id' => $user_id]);
            $call = Call::create($request->all());
            $call->save();
            return dd($call);
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        if ($user->hasRole('owner'))
        {
            $order = Call::findOrFail($id);
            $call = $request->all();
            $order->update($call);
            return dd($order);
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        if ($user->hasRole('owner'))
        {
            $call = Call::findOrfail($id);
            $call->delete();
            return dd($call);
        } else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }
}
