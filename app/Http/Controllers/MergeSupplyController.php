<?php

namespace App\Http\Controllers;

use App\Bid_Sup;
use App\Organization;
use App\Role;
use App\Supply;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;

class MergeSupplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->hasRole('master'))
        {
            $bid_sup = Bid_Sup::with('user', 'supply', 'organizer', 'bid')
                            ->where('supply.id', '=', 'bid_sups.supply_id')
                            ->where('bid.id', '=', 'bid_sups.bid_id')
                            ->where('user.id', '=', 'bid_sups.user_id')
                            ->get();

            dd($bid_sup);
        }
        else if ($user->hasRole('owner'))
        {
            $user_id = Auth::id();
            $bid_sup = Bid_Sup::with('user', 'supply', 'organizer', 'bid')
                        ->where('supply.id', '=', 'bid_sups.supply_id')
                        ->where('bid.id', '=', 'bid_sups.bid_id')
                        ->where('bid_sups.user_id', '=', $user_id)
                        ->get();
            dd($bid_sup);
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if ($user->hasRole('owner'))
        {
            $user_id = Auth::id();
            $request->merge(['user_id' => $user_id]);
            $sup_data = Bid_Sup::create($request->all());

            $contract_file      = $request->file('contract_file');
            if(isset($contract_file)) {
                if (isset($contract_file)) {
                    $extension1 = $request->file('contract_file')->getClientOriginalExtension();
                }
                $sha = 'contract_file' . md5(time());
                if(isset($extension1)){
                    $filename1 = date('Y').$sha . "sgfs." . $extension1;
                }
                $destination_path = 'contracts/';
                if (empty($destination_path)) {
                    File::makeDirectory($destination_path, 0775, true, true);
                }
                if(isset($filename1)){
                    $sup_data->$contract_file = $destination_path . $filename1;
                    $request->file('contracts')->move($destination_path, $filename1);
                    $sup_data->save();
                }
            }
            $all_data =  $request->all();
            $all_data['contract_file']   = !empty($filename1) ? $filename1  : $sup_data->contract_file;
            $sup_data->update($all_data);
            return dd($sup_data);
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        if ($user->hasRole('owner'))
        {
        $order       =  Bid_Sup::findOrFail($id);
        $all_data =  $request->all();
        $order->update($all_data);
        return dd($order);
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        if ($user->hasRole('owner'))
        {
            $meetings = Bid_Sup::findOrfail($id);
            $meetings->delete();
            return dd($meetings);
        }
        else
        {
            return JsonResponse::create(['error' => 'access-denied'], 401);
        }

    }
}
