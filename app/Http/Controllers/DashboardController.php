<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
       $user_email   = Auth::user()->email;
        $user   =Auth::user();
        if($user->hasRole('master'))
        {
            return view('home.master_dashboard');
        }
        elseif ($user->hasRole('owner'))
        {
            return view('home.owner_dashboard');
        }
        elseif ($user->hasRole('admin'))
        {
            return view('home.admin_dashboard');
        }
        elseif ($user->hasRole('client'))
        {
            return view('home.client_dashboard');
        }
        else
        {
            return 'no role';
        }

    }
}
