<?php

namespace Ewawepro\Http\Middleware;

use Closure;
use Auth;
class PleaseLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $user = Auth::user();

            return redirect()->route('/dashboard');
        }
        return $next($request);
    }
}
