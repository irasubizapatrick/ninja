<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organization extends Model
{

    use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $table	=	'organizations';
    protected $fillable	=	['id', 'user_id', 'organization_name','email','address','telephone','website','represented','status'];
}
