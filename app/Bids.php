<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bids extends Model
{

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'bids';
    protected $fillable = ['id', 'user_id', 'organization_id', 'bid_name', 'reference_number', 'items', 'bid_comment', 'bid_status'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *   Function belong to users  Model
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *   Function belong to Organization  Model
     */
    public function organizer()
    {
        return $this->belongsTo('App\Organization');
    }
}
