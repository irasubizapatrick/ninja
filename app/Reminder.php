<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reminder extends Model
{

    use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $table	=	'reminders';
    protected $fillable	=	['id', 'user_id', 'bid__sup_id','reminder_name','reminder_message','reminder_date','reminder_status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *   Function belong to Call Model
     */
    public function bid_sup()
    {
        return $this->belongsTo('App\Bid_Sup');
    }
}

