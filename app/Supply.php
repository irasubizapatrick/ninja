<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supply extends Model
{

    use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $table	=	'supplies';
    protected $fillable	=	['id', 'user_id','supplier_name','supplier_contact','supplier_email','supplier_telephone','supplier_address','supplier_status'];
}