<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Call extends Model
{
    use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $table	=	'calls';
    protected $fillable	=	['id', 'user_id', 'bid__sup_id','call_date','call_reason','call_comment'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *   Function belong to Call Model
     */
    public function bid_sup()
    {
        return $this->belongsTo('App\Bid_Sup');
    }
}
