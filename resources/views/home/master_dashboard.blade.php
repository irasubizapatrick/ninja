<!DOCTYPE html>
<html lang="en">

@include('layouts.head')

<body data-sidebar-skin="dark" data-header-skin="light" data-navbar-brand-skin="dark" data-sidebar-state="expand">
<div id="wrapper" class="wrapper">
    <!-- HEADER & TOP NAVIGATION -->
    @include('layouts.header')
    <!-- /.navbar -->
    <div class="content-wrapper">
        <!-- SIDEBAR -->
    @include('layouts.master_sidebar')
    <!-- /.site-sidebar -->
        <main class="main-wrapper clearfix">
            <!-- Page Title Area -->
            <div class="page-title-expand">
                <div class="container">
                    <div class="row">
                        <div>
                            <h4 class="my-1">Dashboard</h4>
                            </p>
                        </div>
                        <div class="flex-1"></div>
                        <!-- /.d-sm-inline-flex -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /.page-title -->
            <!-- =================================== -->
            <!-- Different data widgets ============ -->
            <!-- =================================== -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="widget-list row">
                            <div class="widget-holder col-6">
                                <div class="widget-bg">
                                    <div class="widget-body bg-success text-inverse px-4 pt-3 pb-4 ">
                                        <div class="icon-box icon-box-centered flex-1 my-0 p-0">
                                            <header class="align-self-center"><a href="#" class="bg-grey fs-30 text-muted"><i class="lnr lnr-enter icon-lg"></i></a>
                                            </header>
                                            <section class="mt-0">
                                                <h6 class="icon-box-title my-0"><span class="counter">337</span></h6>
                                                <p class="mb-0">Contracts</p>
                                            </section>
                                        </div>
                                        <!-- /.icon-box -->
                                    </div>
                                    <!-- /.widget-body -->
                                </div>
                                <!-- /.widget-bg -->
                            </div>
                            <!-- /.widget-holder -->
                            <div class="widget-holder col-6">
                                <div class="widget-bg">
                                    <div class="widget-body bg-secondary text-inverse px-4 pt-3 pb-4">
                                        <div class="icon-box icon-box-centered flex-1 my-0 p-0">
                                            <header class="align-self-center"><a href="#" class="bg-grey fs-30 text-muted"><i class="lnr lnr-book icon-lg"></i></a>
                                            </header>
                                            <section class="mt-0">
                                                <h6 class="icon-box-title my-0">
                                                    <span class="counter">492</span>
                                                </h6>
                                                <p class="mb-0">Bids</p>
                                            </section>
                                        </div>
                                        <!-- /.icon-box -->
                                    </div>
                                    <!-- /.widget-body -->
                                </div>
                                <!-- /.widget-bg -->
                            </div>
                            <!-- /.widget-holder -->
                            <div class="widget-holder col-6">
                                <div class="widget-bg">
                                    <div class="widget-body bg-warning text-inverse px-4 pt-3 pb-4 ">
                                        <div class="icon-box icon-box-centered flex-1 my-0 p-0">
                                            <header class="align-self-center"><a href="#" class="bg-grey fs-30 text-muted"><i class="lnr lnr-users icon-lg"></i></a>
                                            </header>
                                            <section class="mt-0">
                                                <h6 class="icon-box-title my-0"><span class="counter">7248</span></h6>
                                                <p class="mb-0">Public Entity</p>
                                            </section>
                                        </div>
                                        <!-- /.icon-box -->
                                    </div>
                                    <!-- /.widget-body -->
                                </div>
                                <!-- /.widget-bg -->
                            </div>
                            <!-- /.widget-holder -->
                            <div class="widget-holder col-6">
                                <div class="widget-bg">
                                    <div class="widget-body px-4 pt-3 pb-4">
                                        <div class="icon-box icon-box-centered flex-1 my-0 p-0 ">
                                            <header class="align-self-center">
                                                <a href="#" class="bg-grey fs-30 text-muted">
                                                    <i class="lnr lnr-menu icon-lg"></i>
                                                </a>
                                            </header>
                                            <section class="mt-0">
                                                <h6 class="icon-box-title my-0"><span class="counter">020</span></h6>
                                                <p class="mb-0">Users</p>
                                            </section>
                                        </div>
                                        <!-- /.icon-box -->
                                    </div>
                                    <!-- /.widget-body -->
                                </div>
                                <!-- /.widget-bg -->
                            </div>
                            <!-- /.widget-holder -->
                        </div>
                        <!-- /.widget-list -->
                    </div>
                    <!-- /.col-lg-4 -->
                    <div class="col-lg-8">
                        <div class="widget-holder widget-full-height widget-flex">
                            <div class="widget-bg">
                                <div class="widget-body py-3">
                                    <div class="widget-site-visits">
                                        <!-- /.d-flex -->
                                        <h2 class="my-0 lh-10"><span>1829</span></h2>
                                        <p class="text-muted mb-0">Contracts</p>
                                        <div class="mx-auto" style="height: 212px">
                                            <canvas id="chartJsSiteVisits"></canvas>
                                        </div>
                                    </div>
                                    <!-- /.widget-site-visits -->
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                            <!-- /.widget-list -->
                        </div>
                        <!-- /.col-lg-8 -->
                    </div>
                    <!-- /.row -->
                    <div class="widget-list row">
                        <!-- /.widget-holder -->
                        <div class="widget-holder widget-full-height col-lg-6 d-none">
                            <div class="widget-bg">
                                <div class="widget-heading">
                                    <h5 class="widget-title">Latest Orders</h5>
                                    <div class="widget-actions">
                                        <div class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="lnr lnr-cog"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="index.html#">Action</a>  <a class="dropdown-item" href="index.html#">Another action</a>  <a class="dropdown-item" href="index.html#">Something else here</a>
                                            </div>
                                            <!-- /.dropdown-menu -->
                                        </div>
                                        <!-- /.dropdown -->
                                    </div>
                                    <!-- /.widget-actions -->
                                </div>
                                <!-- /.widget-heading -->
                                <div class="widget-body px-0 pb-0 pt-3">
                                    <table class="table table-striped table-streched table-sm">
                                        <thead>
                                        <tr class="text-muted">
                                            <th>Date</th>
                                            <th class="text-right">Sales Count</th>
                                            <th class="text-right">Earning</th>
                                            <th class="text-right">Tax Witheld</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>16 Feb 2019</td>
                                            <td class="text-right">24</td>
                                            <td class="text-right">$387</td>
                                            <td class="text-danger text-right">-$12.50</td>
                                        </tr>
                                        <tr>
                                            <td>15 Feb 2019</td>
                                            <td class="text-right">31</td>
                                            <td class="text-right">$453</td>
                                            <td class="text-danger text-right">-$25</td>
                                        </tr>
                                        <tr>
                                            <td>15 Feb 2019</td>
                                            <td class="text-right">10</td>
                                            <td class="text-right">$156</td>
                                            <td class="text-danger text-right">-$3.50</td>
                                        </tr>
                                        <tr>
                                            <td>13 Feb 2019</td>
                                            <td class="text-right">9</td>
                                            <td class="text-right">$95</td>
                                            <td class="text-danger text-right">-$8.75</td>
                                        </tr>
                                        <tr>
                                            <td>12 Feb 2019</td>
                                            <td class="text-right">28</td>
                                            <td class="text-right">$412</td>
                                            <td class="text-danger text-right">-$15.50</td>
                                        </tr>
                                        <tr>
                                            <td>12 Feb 2019</td>
                                            <td class="text-right">13</td>
                                            <td class="text-right">$278</td>
                                            <td class="text-danger text-right">-$13.75</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!-- /.table -->
                                </div>
                                <!-- /.widget-body -->
                            </div>
                            <!-- /.widget-bg -->
                        </div>
                        <!-- /.widget-holder -->
                    </div>
                </div>
                <!-- /.page-title -->
                <!-- =================================== -->
                <!-- Different data widgets ============ -->
                <!-- =================================== -->
                <div class="container">
                    <div class="widget-list">
                        <div class="row">
                            <div class="col-md-12 widget-holder p-0">
                                <div class="widget-bg">
                                    <!-- /.widget-heading -->
                                    <div class="widget-body clearfix">
                                        <table class="table table-striped table-bordered" data-toggle="datatables" data-plugin-options='{"searching": false}'>
                                            <thead>
                                            <tr>
                                                <th>Bid Name</th>
                                                <th>Reference Number</th>
                                                <th>Derivable</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>Colleen Hurst</td>
                                                <td>Javascript Developer</td>
                                                <td>San Francisco</td>
                                                <td>39</td>
                                                <td>Active</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.widget-body -->
                                </div>
                                <!-- /.widget-bg -->
                            </div>
                            <!-- /.widget-holder -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.widget-list -->
                </div>
                <!-- /.widget-list -->
            </div>
            <!-- /.container -->
        </main>
        <!-- /.main-wrappper -->
        <!-- RIGHT SIDEBAR -->
        <aside class="right-sidebar scrollbar-enabled suppress-x">
            <div class="sidebar-icons row"><a href="index.html#" class="col-4"><i class="lnr lnr-user icon-sm"></i> </a><a href="index.html#" class="col-4"><i class="lnr lnr-cog icon-sm"></i> </a><a href="index.html#" class="col-4 bw-r-0"><i class="lnr lnr-exit icon-sm"></i></a>
            </div>
            <!-- /.right-sidebar-icons -->
            <div class="sidebar-chat mr-t-30" data-plugin="chat-sidebar">
                <div class="sidebar-chat-info">
                    <h6 class="sidebar-title mt-0 mr-l-20">Chat List</h6>
                    <form class="mr-t-10">
                        <div class="form-group">
                            <input type="search" class="form-control form-control-rounded fs-12 heading-font-family pd-l-20 pd-r-30" placeholder="Search for friends ..."> <i class="feather feather-search post-absolute pos-right vertical-center mr-3 text-muted"></i>
                        </div>
                    </form>
                </div>
                <div class="chat-list">
                    <div class="list-group row">
                        <a href="javascript:void(0)" class="list-group-item" data-chat-user="Julein Renvoye">
                            <figure class="thumb-xs user--online mr-3 mr-0-rtl ml-3-rtl">
                                <img src="assets/demo/users/2.jpg" class="rounded-circle" alt="">
                            </figure><span><span class="name">Gene Newman</span>  <span class="username">@gene_newman</span> </span>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item" data-chat-user="Eddie Lebanovkiy">
                            <figure class="thumb-xs user--online mr-3 mr-0-rtl ml-3-rtl">
                                <img src="assets/demo/users/3.jpg" class="rounded-circle" alt="">
                            </figure><span><span class="name">Billy Black</span>  <span class="username">@billyblack</span> </span>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item" data-chat-user="Cameron Moll">
                            <figure class="thumb-xs user--online mr-3 mr-0-rtl ml-3-rtl">
                                <img src="assets/demo/users/5.jpg" class="rounded-circle" alt="">
                            </figure><span><span class="name">Herbert Diaz</span>  <span class="username">@herbert</span> </span>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item" data-chat-user="Bill S Kenny">
                            <figure class="user--busy thumb-xs mr-3 mr-0-rtl ml-3-rtl">
                                <img src="assets/demo/users/4.jpg" class="rounded-circle" alt="">
                            </figure><span><span class="name">Sylvia Harvey</span>  <span class="username">@sylvia</span> </span>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item" data-chat-user="Trent Walton">
                            <figure class="user--busy thumb-xs mr-3 mr-0-rtl ml-3-rtl">
                                <img src="assets/demo/users/1.jpg" class="rounded-circle" alt="">
                            </figure><span><span class="name">Marsha Hoffman</span>  <span class="username">@m_hoffman</span> </span>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item" data-chat-user="Julien Renvoye">
                            <figure class="user--offline thumb-xs mr-3 mr-0-rtl ml-3-rtl">
                                <img src="assets/demo/users/2.jpg" class="rounded-circle" alt="">
                            </figure><span><span class="name">Mason Grant</span>  <span class="username">@masongrant</span> </span>
                        </a>
                        <a href="javascript:void(0)" class="list-group-item" data-chat-user="Eddie Lebaovskiy">
                            <figure class="user--offline thumb-xs mr-3 mr-0-rtl ml-3-rtl">
                                <img src="assets/demo/users/3.jpg" class="rounded-circle" alt="">
                            </figure><span><span class="name">Shelly Sullivan</span>  <span class="username">@shelly</span></span>
                        </a>
                    </div>
                    <!-- /.list-group -->
                </div>
                <!-- /.chat-list -->
            </div>
            <!-- /.sidebar-chat -->
        </aside>
        <!-- CHAT PANEL -->
        <div class="chat-panel" hidden>
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center"><i class="lnr lnr-user text-success mr-3"></i>  <span class="user-name heading-font-family fw-400 flex-1">John Doe</span>
                    <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="widget-chat-activity flex-1">
                        <div class="messages scrollbar-enabled suppress-x">
                            <div class="message media reply">
                                <figure class="thumb-xs2 user--online">
                                    <a href="index.html#">
                                        <img src="assets/demo/users/3.jpg" class="rounded-circle" alt="">
                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>Epic Cheeseburgers come in all kind of styles.</p>
                                </div>
                                <!-- /.message-body -->
                            </div>
                            <!-- /.message -->
                            <div class="message media">
                                <figure class="thumb-xs2 user--online">
                                    <a href="index.html#">
                                        <img src="assets/demo/users/1.jpg" class="rounded-circle" alt="">
                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>Cheeseburgers make your knees weak.</p>
                                </div>
                                <!-- /.message-body -->
                            </div>
                            <!-- /.message -->
                            <div class="message media reply">
                                <figure class="thumb-xs2 user--offline">
                                    <a href="index.html#">
                                        <img src="assets/demo/users/5.jpg" class="rounded-circle" alt="">
                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>Cheeseburgers will never let you down.</p>
                                    <p>They'll also never run around or desert you.</p>
                                </div>
                                <!-- /.message-body -->
                            </div>
                            <!-- /.message -->
                            <div class="message media">
                                <figure class="thumb-xs2 user--online">
                                    <a href="index.html#">
                                        <img src="assets/demo/users/1.jpg" class="rounded-circle" alt="">
                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>A great cheeseburger is a gastronomical event.</p>
                                </div>
                                <!-- /.message-body -->
                            </div>
                            <!-- /.message -->
                            <div class="message media reply">
                                <figure class="thumb-xs2 user--busy">
                                    <a href="index.html#">
                                        <img src="assets/demo/users/6.jpg" class="rounded-circle" alt="">
                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>There's a cheesy incarnation waiting for you no matter what you palete preferences are.</p>
                                </div>
                                <!-- /.message-body -->
                            </div>
                            <!-- /.message -->
                            <div class="message media">
                                <figure class="thumb-xs2 user--online">
                                    <a href="index.html#">
                                        <img src="assets/demo/users/1.jpg" class="rounded-circle" alt="">
                                    </a>
                                </figure>
                                <div class="message-body media-body">
                                    <p>If you are a vegan, we are sorry for you loss.</p>
                                </div>
                                <!-- /.message-body -->
                            </div>
                            <!-- /.message -->
                        </div>
                        <!-- /.messages -->
                    </div>
                    <!-- /.widget-chat-acitvity -->
                </div>
                <!-- /.card-body -->
                <form action="javascript:void(0)" class="card-footer" method="post">
                    <div class="d-flex justify-content-end">
                        <textarea class="border-0 flex-1" rows="1" style="resize: none" placeholder="Type your message here"></textarea>
                        <button class="btn btn-sm btn-circle bg-transparent" type="submit"><i class="lnr lnr-chevron-right list-icon fs-20 text-success"></i>
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
        </div>
        <!-- /.chat-panel -->
    </div>
    <!-- /.content-wrapper -->
    <!-- FOOTER -->
</div>
<!--/ #wrapper -->
<!-- Scripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/metisMenu/2.7.9/metisMenu.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/countup.js/1.9.2/countUp.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqvmap/1.5.1/jquery.vmap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqvmap/1.5.1/maps/jquery.vmap.usa.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="assets/js/initial.min.js"></script>
<script src="assets/js/template.js"></script>
<script src="assets/js/custom.js"></script>
</body>

</html>