<aside class="site-sidebar scrollbar-enabled" data-suppress-scroll-x="true">
    <!-- Sidebar Menu -->
    <nav class="sidebar-nav">
        <div class="profile-sec">
            <div class="profile-cont">
                <img data-name="{{Auth::user()->name}}" class="profile-img"/>
                <p>{{ Auth::user()->name }}</p>
                <span>{{ Auth::user()->email }}</span>
            </div>
        </div>
        <ul class="nav in side-menu">
            <li class="current-page active">
                <a href="/dashboard">
                    <i class="list-icon lnr lnr-home"></i>
                    <span class="hide-menu">Dashboard</span>
                </a>
            </li>
            <li>
                <a href="/list_company">
                    <i class="list-icon lnr lnr-book"></i>
                    <span class="hide-menu">Institutions</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="list-icon lnr lnr-chart-bars"></i>
                    <span class="hide-menu">Reports</span>
                </a>
            </li>
            <li class="menu-item-has-children"><a href="javascript:void(0);"><i class="list-icon lnr lnr-cog"></i> <span class="hide-menu">Settings</span></a>
                <ul class="list-unstyled sub-menu">

                    <li>
                        <a href="#">
                            Profile
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Change Password
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            </form>
                            Sign Out
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- /.side-menu -->
    </nav>
    <!-- /.sidebar-nav -->
</aside>