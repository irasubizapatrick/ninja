<nav class="navbar">
    <div class="container-fluid px-0 align-items-stretch">
        <!-- Logo Area -->
        <div class="navbar-header">
            <a href="#" class="navbar-brand">
                <h1 class="logo">E-Contracts</h1>
            </a>
        </div>
        <!-- /.navbar-left -->
        <!-- /.navbar-search -->
        <div class="spacer"></div>
        <!-- /.navbar-right -->
        <!-- User Image with Dropdown -->
        <ul class="nav navbar-nav">
            <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle dropdown-toggle-user ripple" data-toggle="dropdown"><span class="avatar thumb-xs">
                        <img data-name="{{Auth::user()->name}}" class="small-profile rounded-circle"/>
                        <i class="list-icon lnr lnr-chevron-down"></i></span></a>
                <div
                        class="dropdown-menu dropdown-left dropdown-card dropdown-card-profile animated flipInY">
                    <div class="card">
                        <ul class="list-unstyled card-body">
                            <li>
                                <a href="#">
                                        <span>
                                            <span class="align-middle">
                                            Manage Accounts</span>
                                        </span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                        <span>
                                            <span class="align-middle">
                                            Change Password</span>
                                        </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    </form>
                                    <span>
                                            <span class="align-middle">
                                            Sign Out</span>
                                        </span>
                                </a>
                            </li>
                        </ul>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.dropdown-card-profile -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-nav -->
    </div>
    <!-- /.container -->
</nav>